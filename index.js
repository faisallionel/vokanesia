import { name as appName } from "./app.json";

import React from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";
import splash from "./components/splashScreen";

AppRegistry.registerComponent(appName, () => splash);
