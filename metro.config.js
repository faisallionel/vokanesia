/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 * <!-- made by faisal -->
 * <!-- faisallionel@gmail.com -->
 * <!-- https://faisal.wadahsukses.com -->
 * <!-- https://wadahsukses.com -->
 */

module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
  },
};
