import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  ImageBackground,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Button,
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const slides = [
  {
    key: "one",
    title: "BELAJAR DENGAN\nAKADEMI ONLINE",
    text: "Materi belajar yang luas dan detail akan membantu kamu untuk memahami berbagai pengetahuan.",
    image: require("../assets/hp/hp1.png"),
    backgroundColor: "#04226B",
    color: "#FFFFFF",
  },
  {
    key: "two",
    title: "IKUTI UPDATE\nBERITA TERBARU",
    text: "Terdapat berbagai berita terbaru yang telah dipilih oleh Vokanesia untuk menambah ilmu pengetahuanmu.",
    image: require("../assets/hp/hp2.png"),
    backgroundColor: "#F5F6F8",
    color: "#04226B",
  },
  {
    key: "three",
    title: "IKUTI EVENT-EVENT\nMENARIK",
    text: "Dapatkan informasi dan event menarik yang diselenggarakan oleh Vokanesia.",
    image: require("../assets/hp/hp3.png"),
    backgroundColor: "#04226B",
    color: "#FFFFFF",
  },
  {
    key: "four",
    title: "KETAHUI\nKOMPETENSIMU",
    text: "Vokanesia memberikan umpan balik terhadap peningkatan skill dan perkembangan belajarmu.",
    image: require("../assets/hp/hp4.png"),
    backgroundColor: "#F5F6F8",
    color: "#04226B",
  },
];

export default class splashScreen extends Component {
  // slider: AppIntroSlider | undefined;
  activeIndex = 0;
  state = {
    showRealApp: false,
  };

  goToSlide = () => {
    console.log("clicked");
    this.slider.goToSlide(
      this.activeIndex < slides.length ? this.activeIndex++ : this.activeIndex,
      true
    );
  };
  _renderItem = ({ item }) => {
    return (
      <View
        source={item.image}
        style={{ flex: 1, backgroundColor: item.backgroundColor }}
      >
        <View style={{ flex: 1 }}>
          <Text
            style={{
              color: item.color,
              position: "absolute",
              right: 43,
              top: 40,
              fontSize: 15,
            }}
            onPress={this.goToSlide}
          >
            Lewati
          </Text>
        </View>
        <View style={{ alignItems: "center", flex: 4 }}>
          <Image
            source={item.image}
            style={{
              width: 300,
              resizeMode: "contain",
              position: "absolute",
              bottom: -40,
              zIndex: 0,
            }}
          />
        </View>
      </View>
    );
  };
  _keyExtractor = (item: Item) => item.text;
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  };
  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-arrow-round-forward" color="red" size={24} />
      </View>
    );
  };
  _prevButton = (activeIndex) => {
    if (activeIndex > 0) {
      return (
        <Text
          style={{
            position: "absolute",
            left: 30,
            backgroundColor: "#04226B",
            borderRadius: 17,
            paddingHorizontal: 15,
            paddingVertical: 3,
            color: "white",
            fontSize: 14,
          }}
          onPress={() => this.slider.goToSlide(activeIndex - 1, true)}
        >
          PREV
        </Text>
      );
    } else {
      return null;
    }
  };
  _nextButton = (activeIndex) => {
    if (activeIndex < slides.length - 1) {
      return (
        <Text
          style={{
            position: "absolute",
            right: 30,
            backgroundColor: "#04226B",
            borderRadius: 17,
            paddingHorizontal: 15,
            paddingVertical: 3,
            color: "white",
            fontSize: 14,
          }}
          onPress={() => this.slider.goToSlide(activeIndex + 1, true)}
        >
          NEXT
        </Text>
      );
    } else {
      return null;
    }
  };
  _renderPagination = (activeIndex) => {
    this.activeIndex = activeIndex;
    return (
      <View
        style={[
          styles.paginationContainer,
          {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.0,

            elevation: 24,
          },
        ]}
      >
        <SafeAreaView>
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 24,
                color: "#04226B",
                marginTop: 48,
              }}
            >
              {slides[activeIndex].title}
            </Text>
            <Text
              style={{
                textAlign: "center",
                fontWeight: "800",
                fontSize: 18,
                color: "#04226B",
                marginTop: 48,
                paddingHorizontal: 65,
              }}
            >
              {slides[activeIndex].text}
            </Text>
          </View>
          <View style={[styles.paginationDots, { flexDirection: "row" }]}>
            {this._prevButton(activeIndex)}
            {slides.length > 1 &&
              slides.map((_, i) => (
                <TouchableOpacity
                  key={i}
                  style={[
                    styles.dot,
                    i === activeIndex
                      ? { backgroundColor: "#FFB776" }
                      : { backgroundColor: "#EB7810" },
                  ]}
                  onPress={() => this.slider.goToSlide(i, true)}
                />
              ))}
            {this._nextButton(activeIndex)}
          </View>
        </SafeAreaView>
      </View>
    );
  };

  render() {
    if (this.state.showRealApp) {
      return (
        <View>
          <Text>Faisal</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          {/* <StatusBar translucent backgroundColor="transparent" /> */}
          <AppIntroSlider
            renderItem={this._renderItem}
            renderPagination={this._renderPagination}
            data={slides}
            renderNextButton={this._renderNextButton}
            ref={(ref) => (this.slider = ref)}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
  },
  bigBlue: {
    color: "blue",
    fontWeight: "bold",
    fontSize: 30,
  },
  image: {
    width: 200,
    height: 200,
  },
  slide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "blue",
  },
  image: {
    width: 320,
    height: 320,
    marginVertical: 32,
  },
  text: {
    color: "rgba(255, 255, 255, 0.8)",
    textAlign: "center",
  },
  title: {
    fontSize: 22,
    color: "white",
    textAlign: "center",
  },
  paginationContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 350,
    flex: 1,
    backgroundColor: "white",
    borderTopLeftRadius: 42,
    borderTopRightRadius: 42,
  },
  paginationDots: {
    height: 16,
    margin: 16,
    top: 300,
    bottom: 0,
    left: 0,
    right: 0,
    position: "absolute",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  buttonContainer: {
    flexDirection: "row",
    marginHorizontal: 24,
  },
  button: {
    flex: 1,
    paddingVertical: 20,
    marginHorizontal: 8,
    borderRadius: 24,
    backgroundColor: "#1cb278",
  },
  buttonText: {
    color: "white",
    fontWeight: "600",
    textAlign: "center",
    flex: 1,
  },
});
